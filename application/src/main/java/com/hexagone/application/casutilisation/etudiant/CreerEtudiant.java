package com.hexagone.application.casutilisation.etudiant;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.ports.EtudiantRepositoryPort;
import com.hexagone.application.service.EtudiantService;
import com.hexagone.domain.model.EtudiantModel;

public class CreerEtudiant {
  private final EtudiantRepositoryPort etudiantRepositoryPort;
  private final EtudiantService etudiantService;

  public CreerEtudiant(EtudiantRepositoryPort etudiantRepositoryPort){
    this.etudiantRepositoryPort = etudiantRepositoryPort;
    this.etudiantService = new EtudiantService(etudiantRepositoryPort);
  }
  public EtudiantModel creer(CreerEtudiantCommande creerEtudiantCommande){
    EtudiantModel etudiantModelToCreated = this.etudiantService.convertirCreerEtudiantCommandeVersEtudiantModel(
        creerEtudiantCommande);
    return this.etudiantRepositoryPort.enregistrerEtudiant(etudiantModelToCreated);

  }
}
