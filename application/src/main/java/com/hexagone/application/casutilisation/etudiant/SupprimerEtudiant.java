package com.hexagone.application.casutilisation.etudiant;

import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.application.ports.EtudiantRepositoryPort;
import com.hexagone.application.service.EtudiantService;
import com.hexagone.domain.model.EtudiantModel;

public class SupprimerEtudiant {
  private final EtudiantRepositoryPort etudiantRepositoryPort;
  private final EtudiantService etudiantService;

  public SupprimerEtudiant(EtudiantRepositoryPort etudiantRepositoryPort) {
    this.etudiantRepositoryPort = etudiantRepositoryPort;
    this.etudiantService = new EtudiantService(etudiantRepositoryPort);
  }

  public EtudiantModel supprimerEtudiant(SupprimerEtudiantCommande commande){
    Long id = commande.id();
    this.etudiantRepositoryPort.obtenirEtudiantParId(id).orElseThrow();
    return this.etudiantRepositoryPort.supprimerEtudiant(id);
  }

}
