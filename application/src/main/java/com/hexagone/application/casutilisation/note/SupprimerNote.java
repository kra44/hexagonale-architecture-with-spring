package com.hexagone.application.casutilisation.note;

import com.hexagone.application.commande.note.SupprimerNoteCommande;
import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.application.service.NoteService;
import com.hexagone.domain.model.NoteModel;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class SupprimerNote {
  private final NoteRepositoryPort noteRepositoryPort;
  private final NoteService noteService;

  public SupprimerNote(NoteRepositoryPort noteRepositoryPort) {
    this.noteRepositoryPort = noteRepositoryPort;
    this.noteService = new NoteService(noteRepositoryPort);
  }
  public NoteModel supprimerNote(SupprimerNoteCommande commande){
    Long id = commande.id();
    this.noteRepositoryPort.obtenirNoteParId(id);
    return this.noteRepositoryPort.supprimerNote(id);
  }

}

