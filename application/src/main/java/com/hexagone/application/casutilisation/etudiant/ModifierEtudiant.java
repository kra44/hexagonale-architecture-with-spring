package com.hexagone.application.casutilisation.etudiant;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.ports.EtudiantRepositoryPort;
import com.hexagone.application.service.EtudiantService;
import com.hexagone.domain.model.EtudiantModel;

public class ModifierEtudiant {
  private final EtudiantRepositoryPort etudiantRepositoryPort;
  private final EtudiantService etudiantService;

  public ModifierEtudiant(EtudiantRepositoryPort etudiantRepositoryPort) {
    this.etudiantRepositoryPort = etudiantRepositoryPort;
    this.etudiantService = new EtudiantService(etudiantRepositoryPort);
  }

  public EtudiantModel modifier(CreerEtudiantCommande creerEtudiantCommande){
    EtudiantModel etudiantModelToCreated = this.etudiantService.convertirCreerEtudiantCommandeVersEtudiantModel(
        creerEtudiantCommande);
     return this.etudiantRepositoryPort.enregistrerEtudiant(etudiantModelToCreated);
  }
}
