package com.hexagone.application.casutilisation.note;

import com.hexagone.application.commande.note.CreeNoteCommande;
import com.hexagone.application.commande.note.ModifierNoteCommande;
import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.application.service.NoteService;
import com.hexagone.domain.model.NoteModel;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class ModifierNote {
  private final NoteRepositoryPort noteRepositoryPort;
  private final NoteService noteService;

  public ModifierNote(NoteRepositoryPort noteRepositoryPort) {
    this.noteRepositoryPort = noteRepositoryPort;
    this.noteService = new NoteService(noteRepositoryPort);
  }
  public NoteModel modifier(CreeNoteCommande commande) {
    NoteModel noteModelToCreated = this.noteService.convertirCreerEtudiantCommandeVersEtudiantModel(
        commande);
    return this.noteRepositoryPort.modifierNote(noteModelToCreated);
  }

}

