package com.hexagone.application.ports;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;
import java.util.List;
import java.util.Optional;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public interface NoteRepositoryPort {
  NoteModel enregistrerNote(NoteModel noteModel);
  NoteModel modifierNote(NoteModel noteModel);
  Optional<NoteModel> obtenirNoteParId(Long idNote);
  List<NoteModel> obtenirListNote();
  NoteModel supprimerNote(Long idNote);
}

