package com.hexagone.application.ports;

import com.hexagone.domain.model.EtudiantModel;
import java.util.List;
import java.util.Optional;

public interface EtudiantRepositoryPort {
  EtudiantModel enregistrerEtudiant(EtudiantModel etudiantModel);
  EtudiantModel modifierEtudiant(EtudiantModel etudiantModel);
  Optional<EtudiantModel> obtenirEtudiantParId(Long idEtudiant);
  List<EtudiantModel> obtenirListEtudiant();
  EtudiantModel supprimerEtudiant(Long idEtudiant);

}
