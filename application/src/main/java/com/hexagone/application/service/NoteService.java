package com.hexagone.application.service;

import com.hexagone.application.casutilisation.note.CreerNote;
import com.hexagone.application.commande.note.CreeNoteCommande;
import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public record NoteService(NoteRepositoryPort noteRepositoryPort) {
  public NoteModel convertirCreerEtudiantCommandeVersEtudiantModel(
      CreeNoteCommande commande){
    return new NoteModel(commande.getId(), commande.getNote(), commande.getEtudiant(), commande.getCreateDate(), commande.getUpdateDate(),commande.getStatus());
  }
}

