package com.hexagone.application.service;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.ports.EtudiantRepositoryPort;
import com.hexagone.domain.model.EtudiantModel;

public record EtudiantService(EtudiantRepositoryPort etudiantRepositoryPort) {
  public EtudiantModel convertirCreerEtudiantCommandeVersEtudiantModel(CreerEtudiantCommande commande){
    return new EtudiantModel(commande.getId(), commande.getFirstName(), commande.getLastName(),
        commande.getContact(), commande.getCreateDate(), commande.getUpdateDate(),commande.getStatus());
  }

}
