package com.hexagone.application.utils;

import com.hexagone.application.casutilisation.note.CreerNote;
import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.application.commande.note.CreeNoteCommande;
import com.hexagone.application.commande.note.ModifierNoteCommande;
import com.hexagone.application.commande.note.SupprimerNoteCommande;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

public class Converter {
  public static EtudiantModel convertiCeerEtudiantCommandeVersEtudiantModel(CreerEtudiantCommande commande){
    return new EtudiantModel(
        commande.getId(),
        commande.getFirstName(),
        commande.getLastName(),
        commande.getContact(),
        commande.getCreateDate(),
        commande.getUpdateDate(),
        commande.getStatus()
    );
  }

  public static CreerEtudiantCommande convertirEtudiantModelVersCreerCommande(EtudiantModel etudiantModel){
    CreerEtudiantCommande commande = new CreerEtudiantCommande();
    commande.setId(etudiantModel.getId());
    commande.setContact(etudiantModel.getContact());
    commande.setFirstName(etudiantModel.getFirstName());
    commande.setStatus(etudiantModel.getStatus());
    commande.setLastName(etudiantModel.getLastName());
    commande.setCreateDate(etudiantModel.getCreateDate());
    commande.setUpdateDate(etudiantModel.getUpdateDate());
    return commande;
  }

  public static EtudiantModel convertiModifierVersEtudiantModel(ModifierEtudiantCommande commande){
    return new EtudiantModel(
        commande.getId(),
        commande.getFirstName(),
        commande.getLastName(),
        commande.getContact(),
        commande.getCreateDate(),
        commande.getUpdateDate(),
        commande.getStatus()
    );
  }

  public static ModifierEtudiantCommande convertirVersModifierEtudiantCommande(EtudiantModel etudiantModel){
    ModifierEtudiantCommande commande = new ModifierEtudiantCommande(etudiantModel.getId());
    commande.setId(etudiantModel.getId());
    commande.setContact(etudiantModel.getContact());
    commande.setFirstName(etudiantModel.getFirstName());
    commande.setStatus(etudiantModel.getStatus());
    commande.setLastName(etudiantModel.getLastName());
    commande.setCreateDate(etudiantModel.getCreateDate());
    commande.setUpdateDate(etudiantModel.getUpdateDate());
    return commande;
  }

  public static EtudiantModel convertirSupprimerEtudiantVersEtudiantModel(
      SupprimerEtudiantCommande commande){
    return new EtudiantModel(commande.id());
  }
  public static SupprimerEtudiantCommande convertirEtudiantModelVersSupprimerEtudiant(
      EtudiantModel etudiantModel){
    SupprimerEtudiantCommande commande = new SupprimerEtudiantCommande(etudiantModel.getId());
    return commande;
  }

  public static CreeNoteCommande convertirNoteModelVersCreerNoteCommande(NoteModel noteModel){
    CreeNoteCommande commande = new CreeNoteCommande();
    commande.setId(noteModel.getId());
    commande.setNote(noteModel.getNote());
    commande.setEtudiant(noteModel.getEtudiant());
    commande.setCreateDate(noteModel.getCreateDate());
    commande.setUpdateDate(noteModel.getUpdateDate());
    return commande;
  }

  public static NoteModel convertirCreerNoteVersNoteModel(CreeNoteCommande commande){
    return new NoteModel(
        commande.getId(),
        commande.getNote(),
        commande.getEtudiant(),
        commande.getCreateDate(),
        commande.getUpdateDate(),
        commande.getStatus()
    );
  }


  public static ModifierNoteCommande convertirNoteModelVersModifierNoteCommande(NoteModel noteModel){
    ModifierNoteCommande commande = new ModifierNoteCommande(noteModel.getId());
    commande.setId(noteModel.getId());
    commande.setNote(noteModel.getNote());
    commande.setEtudiant(noteModel.getEtudiant());
    commande.setCreateDate(noteModel.getCreateDate());
    commande.setUpdateDate(noteModel.getUpdateDate());
    return commande;
  }

  public static NoteModel convertirModifierCommandeNoteVersNoteModel(ModifierNoteCommande commande){
    return new NoteModel(
        commande.getId(),
        commande.getNote(),
        commande.getEtudiant(),
        commande.getCreateDate(),
        commande.getUpdateDate(),
        commande.getStatus()
    );
  }

  public static NoteModel convertirSupprimerNoteVersNoteModel(
      SupprimerNoteCommande commande){
    return new NoteModel(commande.id());
  }

  public static SupprimerNoteCommande convertirNoteModelVersSupprimerNote(
      NoteModel noteModel){
    SupprimerNoteCommande commande = new SupprimerNoteCommande(noteModel.getId());
    return commande;
  }


}

