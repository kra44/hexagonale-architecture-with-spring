package com.hexagone.application.commande.note;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public record SupprimerNoteCommande(Long id) {

}

