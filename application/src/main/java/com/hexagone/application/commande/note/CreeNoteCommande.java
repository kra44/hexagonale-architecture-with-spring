package com.hexagone.application.commande.note;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;
import java.time.Instant;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class CreeNoteCommande {
  private Long id;
  private Double note;
  private EtudiantModel etudiant;
  private Instant createDate;
  private Instant updateDate;
  private Integer status;

  public CreeNoteCommande() {
  }

  public CreeNoteCommande(Long id, Double note, EtudiantModel etudiant, Instant createDate,
      Instant updateDate, Integer status) {
    this.id = id;
    this.note = note;
    this.etudiant = etudiant;
    this.createDate = createDate;
    this.updateDate = updateDate;
    this.status = status;
  }

  public CreeNoteCommande(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getNote() {
    return note;
  }

  public void setNote(Double note) {
    this.note = note;
  }

  public EtudiantModel getEtudiant() {
    return etudiant;
  }

  public void setEtudiant(EtudiantModel etudiant) {
    this.etudiant = etudiant;
  }

  public Instant getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Instant createDate) {
    this.createDate = createDate;
  }

  public Instant getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Instant updateDate) {
    this.updateDate = updateDate;
  }

  public Integer getStatus() {
    return status;
  }

  public NoteModel convertirNote(NoteModel noteModel){
    noteModel.setId(id);
    noteModel.setNote(note);
    noteModel.setEtudiant(etudiant);
    noteModel.setCreateDate(createDate);
    noteModel.setUpdateDate(updateDate);
    noteModel.setStatus(status);
    return noteModel;
  }

}

