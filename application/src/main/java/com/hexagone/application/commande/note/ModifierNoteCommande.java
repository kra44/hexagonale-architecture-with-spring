package com.hexagone.application.commande.note;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;
import java.beans.ConstructorProperties;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class ModifierNoteCommande extends CreeNoteCommande{
  private final Long id;
  @ConstructorProperties("id")
  public ModifierNoteCommande(Long id) {
    this.id = id;
  }

  @Override
  public Long getId() {
    return id;
  }
  public NoteModel convertirVersNote(){
    return convertirNote(new NoteModel(id));
  }

}

