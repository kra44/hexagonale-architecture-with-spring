package com.hexagone.application.commande.etudiant;

import com.hexagone.domain.model.EtudiantModel;
import java.beans.ConstructorProperties;

public class ModifierEtudiantCommande extends CreerEtudiantCommande{
  private final Long id;
  @ConstructorProperties("id")
  public ModifierEtudiantCommande(Long id) {
    this.id = id;
  }

  @Override
  public Long getId() {
    return id;
  }
  public EtudiantModel convertirVersEtudiant(){
    return convertirEtudiant(new EtudiantModel(id));
  }


}
