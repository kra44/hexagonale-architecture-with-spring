package com.hexagone.application.commande.etudiant;

import com.hexagone.domain.model.EtudiantModel;
import java.time.Instant;

public class CreerEtudiantCommande {

  private Long id;
  private String firstName;
  private String lastName;
  private String contact;
  private Instant createDate;
  private Instant updateDate;
  private Integer status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public Instant getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Instant createDate) {
    this.createDate = createDate;
  }

  public Instant getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Instant updateDate) {
    this.updateDate = updateDate;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public EtudiantModel convertirEtudiant(EtudiantModel etudiantModel){
    etudiantModel.setId(id);
    etudiantModel.setContact(contact);
    etudiantModel.setFirstName(firstName);
    etudiantModel.setLastName(lastName);
    etudiantModel.setCreateDate(createDate);
    etudiantModel.setUpdateDate(updateDate);
    etudiantModel.setStatus(status);
    return etudiantModel;
  }

}
