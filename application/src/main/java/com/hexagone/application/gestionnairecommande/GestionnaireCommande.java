package com.hexagone.application.gestionnairecommande;

public interface GestionnaireCommande<T>{
   T execute(T var);
}
