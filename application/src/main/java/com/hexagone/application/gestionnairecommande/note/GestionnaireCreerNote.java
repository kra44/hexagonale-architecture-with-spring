package com.hexagone.application.gestionnairecommande.note;

import com.hexagone.application.casutilisation.note.CreerNote;
import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.commande.note.CreeNoteCommande;
import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.application.utils.Converter;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class GestionnaireCreerNote implements GestionnaireCommande<CreeNoteCommande> {
  private final CreerNote creerNote;

  public GestionnaireCreerNote(NoteRepositoryPort noteRepositoryPort) {
    this.creerNote = new CreerNote(noteRepositoryPort);
  }

  @Override
  public CreeNoteCommande execute(CreeNoteCommande commande) {
    CreeNoteCommande creeNoteCommande = Converter.convertirNoteModelVersCreerNoteCommande(this.creerNote.creer(commande));
    return creeNoteCommande;
  }
}

