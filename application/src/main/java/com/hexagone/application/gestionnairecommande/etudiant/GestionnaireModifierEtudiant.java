package com.hexagone.application.gestionnairecommande.etudiant;

import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.utils.Converter;
import com.hexagone.application.casutilisation.etudiant.ModifierEtudiant;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.ports.EtudiantRepositoryPort;

public class GestionnaireModifierEtudiant implements
    GestionnaireCommande<ModifierEtudiantCommande> {
  private final ModifierEtudiant modifierEtudiant;

  public GestionnaireModifierEtudiant(EtudiantRepositoryPort etudiantRepositoryPort){
    this.modifierEtudiant = new ModifierEtudiant(etudiantRepositoryPort);
  }

  @Override
  public ModifierEtudiantCommande execute(ModifierEtudiantCommande commande) {
    ModifierEtudiantCommande modifierEtudiantCommande = Converter.convertirVersModifierEtudiantCommande(
        this.modifierEtudiant.modifier(commande));
    return modifierEtudiantCommande;
  }

}
