package com.hexagone.application.gestionnairecommande.note;

import com.hexagone.application.casutilisation.note.SupprimerNote;
import com.hexagone.application.commande.note.SupprimerNoteCommande;
import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.application.utils.Converter;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class GestionnaireSupprimerNote implements GestionnaireCommande<SupprimerNoteCommande> {
  private final SupprimerNote supprimerNote;

  public GestionnaireSupprimerNote(NoteRepositoryPort noteRepositoryPort) {
    this.supprimerNote = new SupprimerNote(noteRepositoryPort);
  }

  @Override
  public SupprimerNoteCommande execute(SupprimerNoteCommande commande) {
    return Converter.convertirNoteModelVersSupprimerNote(
        this.supprimerNote.supprimerNote(commande)
    );
  }
}

