package com.hexagone.application.gestionnairecommande.etudiant;

import com.hexagone.application.casutilisation.etudiant.SupprimerEtudiant;
import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.ports.EtudiantRepositoryPort;
import com.hexagone.application.utils.Converter;

public class GestionnaireSupprimerEtudiant implements
    GestionnaireCommande<SupprimerEtudiantCommande> {

  private final SupprimerEtudiant supprimerEtudiant;

  public GestionnaireSupprimerEtudiant(EtudiantRepositoryPort etudiantRepositoryPort){
    this.supprimerEtudiant = new SupprimerEtudiant(etudiantRepositoryPort);
  }
  @Override
  public SupprimerEtudiantCommande execute(SupprimerEtudiantCommande commande) {
    return Converter.convertirEtudiantModelVersSupprimerEtudiant(
        this.supprimerEtudiant.supprimerEtudiant(commande)
    );
  }
}
