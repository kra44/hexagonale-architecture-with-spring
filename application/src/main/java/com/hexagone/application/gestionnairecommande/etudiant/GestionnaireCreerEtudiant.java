package com.hexagone.application.gestionnairecommande.etudiant;

import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.utils.Converter;
import com.hexagone.application.casutilisation.etudiant.CreerEtudiant;
import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.ports.EtudiantRepositoryPort;

public class GestionnaireCreerEtudiant implements GestionnaireCommande<CreerEtudiantCommande> {
 private final CreerEtudiant creerEtudiant;

 public GestionnaireCreerEtudiant(EtudiantRepositoryPort etudiantRepositoryPort){
   this.creerEtudiant = new CreerEtudiant(etudiantRepositoryPort);
 }

  @Override
  public CreerEtudiantCommande execute(CreerEtudiantCommande commande) {
    CreerEtudiantCommande creerEtudiantCommande =Converter.convertirEtudiantModelVersCreerCommande(this.creerEtudiant.creer(commande));
     return creerEtudiantCommande;
  }
}
