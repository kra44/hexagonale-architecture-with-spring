package com.hexagone.application.gestionnairecommande;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

public interface GestionnaireRequete<T, R>{
  T requete(R r);
}
