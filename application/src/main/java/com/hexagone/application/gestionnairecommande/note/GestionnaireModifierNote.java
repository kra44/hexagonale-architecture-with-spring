package com.hexagone.application.gestionnairecommande.note;

import com.hexagone.application.casutilisation.note.ModifierNote;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.commande.note.ModifierNoteCommande;
import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.application.utils.Converter;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class GestionnaireModifierNote implements GestionnaireCommande<ModifierNoteCommande> {
  private final ModifierNote modifierNote;

  public GestionnaireModifierNote(NoteRepositoryPort noteRepositoryPort) {
    this.modifierNote = new ModifierNote(noteRepositoryPort);
  }

  @Override
  public ModifierNoteCommande execute(ModifierNoteCommande commande) {
    ModifierNoteCommande modifierNoteCommande = Converter.convertirNoteModelVersModifierNoteCommande(
        this.modifierNote.modifier(commande));
    return modifierNoteCommande;
  }
}

