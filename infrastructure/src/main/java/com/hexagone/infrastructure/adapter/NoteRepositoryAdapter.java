package com.hexagone.infrastructure.adapter;

import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.domain.exception.EtudiantException;
import com.hexagone.domain.model.NoteModel;
import com.hexagone.infrastructure.entity.EtudiantEntity;
import com.hexagone.infrastructure.entity.NoteEntity;
import com.hexagone.infrastructure.mapper.NoteMapper;
import com.hexagone.infrastructure.repository.NoteRepository;
import com.hexagone.infrastructure.validator.EtudiantValidator;
import com.hexagone.infrastructure.validator.NoteValidator;
import jakarta.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */
@Component
@Transactional
public class NoteRepositoryAdapter implements NoteRepositoryPort {
  private final NoteMapper noteMapper;
  private final NoteRepository noteRepository;

  public NoteRepositoryAdapter(NoteRepository noteRepository) {
    this.noteRepository = noteRepository;
    this.noteMapper = NoteMapper.INSTANCE;
  }

  @Override
  public NoteModel enregistrerNote(NoteModel noteModel) {
    noteModel.setStatus(1);
    noteModel.setCreateDate(new Date().toInstant());
    noteModel.setUpdateDate(new Date().toInstant());

    String errors = NoteValidator.validate(noteModel);
    if(errors != null){
      throw new EtudiantException(errors);
    }
    NoteEntity noteEntity = this.noteMapper.noteModelVersNoteEntity(noteModel);
    NoteEntity etudiant = this.noteRepository.save(noteEntity);
    return this.noteMapper.noteEntityVersNoteModel(etudiant);
  }

  @Override
  public NoteModel modifierNote(NoteModel noteModel) {
    String errors = NoteValidator.validate(noteModel);
    if(errors != null){
      throw new EtudiantException(errors);
    }
    NoteEntity noteEntity = this.noteMapper.noteModelVersNoteEntity(noteModel);
    NoteEntity noteSaved = this.noteRepository.save(noteEntity);
    String errorData = NoteValidator.ValidateData(noteSaved);
    if(errorData != null){
      throw new EtudiantException(errorData);
    }
    return this.noteMapper.noteEntityVersNoteModel(noteSaved);
  }

  @Override
  public Optional<NoteModel> obtenirNoteParId(Long idNote) {
    return Optional.of(this.noteMapper.noteEntityVersNoteModel(
        this.noteRepository.findById(idNote).orElseThrow()
    ));
  }

  @Override
  public List<NoteModel> obtenirListNote() {
    return this.noteRepository.findAll().stream()
        .map(this.noteMapper::noteEntityVersNoteModel)
        .collect(Collectors.toList());
  }

  @Override
  public NoteModel supprimerNote(Long idNote) {
    this.noteRepository.deleteById(idNote);
    return null;
  }
}

