package com.hexagone.infrastructure.adapter;

import com.hexagone.application.ports.EtudiantRepositoryPort;
import com.hexagone.domain.exception.EtudiantException;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.infrastructure.entity.EtudiantEntity;
import com.hexagone.infrastructure.mapper.EtudiantMapper;
import com.hexagone.infrastructure.repository.EtudiantRepository;
import com.hexagone.infrastructure.validator.EtudiantValidator;
import jakarta.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class EtudiantRepositoryAdapter implements EtudiantRepositoryPort {
  private final EtudiantMapper etudiantMapper;
  private final EtudiantRepository etudiantRepository;

  public EtudiantRepositoryAdapter(EtudiantRepository etudiantRepository) {
    this.etudiantMapper = EtudiantMapper.INSTANCE;
    this.etudiantRepository = etudiantRepository;
  }

  @Override
  public EtudiantModel enregistrerEtudiant(EtudiantModel etudiantModel) {
    etudiantModel.setStatus(1);
    etudiantModel.setCreateDate(new Date().toInstant());
    etudiantModel.setUpdateDate(new Date().toInstant());

    String errors = EtudiantValidator.validate(etudiantModel);
    if(errors != null){
      throw new EtudiantException(errors);
    }
    EtudiantEntity etudiantEntity = this.etudiantMapper.etudiantModelVersEtudiantEntity(etudiantModel);
    EtudiantEntity etudiant = this.etudiantRepository.save(etudiantEntity);
    return this.etudiantMapper.etudiantEntityVersEtudiantModel(etudiant);

  }

  @Override
  public EtudiantModel modifierEtudiant(EtudiantModel etudiantModel) {
    String errors = EtudiantValidator.validate(etudiantModel);
    if(errors != null){
      throw new EtudiantException(errors);
    }
    EtudiantEntity etudiantEntity = this.etudiantMapper.etudiantModelVersEtudiantEntity(etudiantModel);
    EtudiantEntity etudiant = this.etudiantRepository.save(etudiantEntity);
    String errorData = EtudiantValidator.ValidateData(etudiant);
    if(errorData != null){
      throw new EtudiantException(errorData);
    }
    return this.etudiantMapper.etudiantEntityVersEtudiantModel(etudiant);
  }

  @Override
  public Optional<EtudiantModel> obtenirEtudiantParId(Long idEtudiant) {
    return Optional.of(this.etudiantMapper.etudiantEntityVersEtudiantModel(
        this.etudiantRepository.findById(idEtudiant).get()
    ));
  }

  @Override
  public List<EtudiantModel> obtenirListEtudiant() {
    return this.etudiantRepository.findAll().stream()
        .map(this.etudiantMapper::etudiantEntityVersEtudiantModel)
        .collect(Collectors.toList());
  }

  @Override
  public EtudiantModel supprimerEtudiant(Long idEtudiant) {
  this.etudiantRepository.deleteById(idEtudiant);
  return null;
  }
}
