package com.hexagone.infrastructure.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */
@Data
@Entity
@Table(name = "note")
public class NoteEntity extends AbstractionEntity{
  @Column(name = "note")
  private Double note;
  @ManyToOne
  @JoinColumn(name="etudiantId")
  private EtudiantEntity etudiant;

}

