package com.hexagone.infrastructure.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */
@Configuration
@EnableJpaRepositories(basePackages ={"com.hexagone.infrastructure.repository"})
@EntityScan(basePackages = {"com.hexagone.infrastructure.entity"})
@ComponentScan(basePackages = {"com.hexagone.infrastructure"})
public class RepositoryConfig {

}

