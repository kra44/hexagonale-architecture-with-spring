package com.hexagone.infrastructure.service;
import com.hexagone.application.gestionnairecommande.GestionnaireRequete;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.infrastructure.adapter.EtudiantRepositoryAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/**
 * @created 11/05/2023
 * @project architecture
 * @author daniel.kouame
 */

@Service
public class GetEtudiantByCriteriaService implements GestionnaireRequete<List<EtudiantModel>, EtudiantModel> {
  private final EtudiantRepositoryAdapter etudiantRepositoryAdapter;

  public GetEtudiantByCriteriaService(EtudiantRepositoryAdapter etudiantRepositoryAdapter) {
    this.etudiantRepositoryAdapter = etudiantRepositoryAdapter;

  }

  @Override
  public List<EtudiantModel> requete(EtudiantModel etudiantModel) {
    List<EtudiantModel> etudiantModelList = new ArrayList<>();
    if(etudiantModel != null){
      if(etudiantModel.getId() != null){
        Optional<EtudiantModel> etudiantModelData = this.etudiantRepositoryAdapter.obtenirEtudiantParId(etudiantModel.getId());
        if (etudiantModelData != null && etudiantModelData.isPresent()){
          etudiantModelList.add(etudiantModelData.get());
        }
      }else
        etudiantModelList = this.etudiantRepositoryAdapter.obtenirListEtudiant();
    }else{
      etudiantModelList = this.etudiantRepositoryAdapter.obtenirListEtudiant();
    }
   return etudiantModelList;
  }
}

