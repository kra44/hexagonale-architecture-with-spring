package com.hexagone.infrastructure.service;

import com.hexagone.application.gestionnairecommande.GestionnaireRequete;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;
import com.hexagone.infrastructure.adapter.EtudiantRepositoryAdapter;
import com.hexagone.infrastructure.adapter.NoteRepositoryAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class GetNoteByCrietria implements GestionnaireRequete<List<NoteModel>, NoteModel> {

  private final NoteRepositoryAdapter noteRepositoryAdapter;

  public GetNoteByCrietria(NoteRepositoryAdapter noteRepositoryAdapter) {
    this.noteRepositoryAdapter = noteRepositoryAdapter;

  }
  @Override
  public List<NoteModel> requete(NoteModel noteModel) {
    List<NoteModel> noteModelList = new ArrayList<>();
    if(noteModel != null){
      if(noteModel.getId() != null){
        Optional<NoteModel> noteModelData = this.noteRepositoryAdapter.obtenirNoteParId(noteModel.getId());
        if (noteModelData != null && noteModelData.isPresent()){
          noteModelList.add(noteModelData.get());
        }
      }else
        noteModelList = this.noteRepositoryAdapter.obtenirListNote();
    }else{
      noteModelList = this.noteRepositoryAdapter.obtenirListNote();
    }
    return noteModelList;
  }
}

