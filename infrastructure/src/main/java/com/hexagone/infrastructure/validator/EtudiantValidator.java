package com.hexagone.infrastructure.validator;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.infrastructure.entity.EtudiantEntity;
import java.util.List;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class EtudiantValidator {
  public static String validate(EtudiantModel etudiantModel){
    String errors = null;
    if(etudiantModel == null){
      errors = "Donnée inalide !";

    }else if(etudiantModel.getLastName() == null){
      errors = "Le prénom est requis";

    }else if(etudiantModel.getFirstName() == null){
      errors = "Le nom est requis";

    }else if(etudiantModel.getContact() == null){
      errors = "Le contact est requis";
    }
    return errors;
  }

  public static String ValidateDataList(List<EtudiantModel> etudiantModelList){
    String errors = null;
    if(etudiantModelList.isEmpty()){
      errors = "List vide !";
    }
    return errors;
  }

  public static String ValidateData(EtudiantEntity etudiantEntity){
    String errors = null;
    if(etudiantEntity == null){
      errors = "Aucune donnée disponible";
    }
    return errors;
  }

}

