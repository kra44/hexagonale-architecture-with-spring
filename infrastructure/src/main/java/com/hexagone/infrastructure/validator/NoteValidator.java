package com.hexagone.infrastructure.validator;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;
import com.hexagone.infrastructure.entity.EtudiantEntity;
import com.hexagone.infrastructure.entity.NoteEntity;
import java.util.List;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class NoteValidator {
  public static String validate(NoteModel noteModel){
    String errors = null;
    if(noteModel == null){
      errors = "Donnée inalide !";

    }else if(noteModel.getNote() == null){
      errors = "La note est requise";

    }else if(noteModel.getEtudiant().getId() == null){
      errors = "Identifiant null";

    }
    return errors;
  }

  public static String ValidateDataList(List<NoteModel> noteModelList){
    String errors = null;
    if(noteModelList.isEmpty()){
      errors = "List vide !";
    }
    return errors;
  }

  public static String ValidateData(NoteEntity noteEntity){
    String errors = null;
    if(noteEntity == null){
      errors = "Aucune donnée disponible";
    }
    return errors;
  }

}

