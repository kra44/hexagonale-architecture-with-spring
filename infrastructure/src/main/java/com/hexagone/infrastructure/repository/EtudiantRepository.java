package com.hexagone.infrastructure.repository;

import com.hexagone.infrastructure.entity.EtudiantEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EtudiantRepository extends JpaRepository<EtudiantEntity, Long> {

}
