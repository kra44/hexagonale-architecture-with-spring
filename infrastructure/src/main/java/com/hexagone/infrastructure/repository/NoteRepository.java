package com.hexagone.infrastructure.repository;

import com.hexagone.infrastructure.entity.NoteEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */
@Repository
@Transactional
public interface NoteRepository extends JpaRepository<NoteEntity, Long> {

}

