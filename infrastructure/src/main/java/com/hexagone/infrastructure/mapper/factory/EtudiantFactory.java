package com.hexagone.infrastructure.mapper.factory;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.infrastructure.entity.EtudiantEntity;
import org.mapstruct.ObjectFactory;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */
public class EtudiantFactory {

  @ObjectFactory
  public EtudiantModel factory(EtudiantEntity etudiantEntity){
    return new EtudiantModel(etudiantEntity.getId());
  }
}

