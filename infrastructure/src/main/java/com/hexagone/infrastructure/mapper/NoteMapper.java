package com.hexagone.infrastructure.mapper;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;
import com.hexagone.infrastructure.entity.EtudiantEntity;
import com.hexagone.infrastructure.entity.NoteEntity;
import com.hexagone.infrastructure.mapper.factory.NoteFactory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

@Mapper(uses = {NoteFactory.class})
public abstract class NoteMapper {

  public static NoteMapper INSTANCE = Mappers.getMapper(NoteMapper.class);
  @Mapping(target = "etudiant", ignore = true)
  public abstract NoteModel noteEntityVersNoteModel(NoteEntity noteEntity);
  @Mapping(target = "etudiant", ignore = true)
  public abstract NoteEntity noteModelVersNoteEntity(NoteModel noteModel);
}

