package com.hexagone.infrastructure.mapper.factory;

import com.hexagone.domain.model.NoteModel;
import com.hexagone.infrastructure.entity.NoteEntity;
import org.mapstruct.ObjectFactory;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public class NoteFactory {
  @ObjectFactory
  public NoteModel NoteFactory(NoteEntity noteEntity){
    return new NoteModel(noteEntity.getId());
  }

}

