package com.hexagone.infrastructure.mapper;

import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.infrastructure.entity.EtudiantEntity;
import com.hexagone.infrastructure.mapper.factory.EtudiantFactory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

@Mapper(uses = {EtudiantFactory.class})
public abstract class EtudiantMapper {


  public static final EtudiantMapper INSTANCE = Mappers.getMapper(EtudiantMapper.class);
  public abstract EtudiantModel etudiantEntityVersEtudiantModel(EtudiantEntity etudiantEntity);
  public abstract EtudiantEntity etudiantModelVersEtudiantEntity(EtudiantModel etudiantModel);


}

