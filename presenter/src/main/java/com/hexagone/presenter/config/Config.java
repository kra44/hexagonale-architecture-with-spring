package com.hexagone.presenter.config;

import com.hexagone.application.gestionnairecommande.etudiant.GestionnaireCreerEtudiant;
import com.hexagone.application.gestionnairecommande.etudiant.GestionnaireModifierEtudiant;
import com.hexagone.application.gestionnairecommande.etudiant.GestionnaireSupprimerEtudiant;
import com.hexagone.application.ports.EtudiantRepositoryPort;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.hexagone.infrastructure.config.RepositoryConfig;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

@Configuration
@Import(value = {RepositoryConfig.class})
public class Config {

  private final EtudiantRepositoryPort etudiantRepositoryPort;

  public Config(EtudiantRepositoryPort etudiantRepositoryPort) {
    this.etudiantRepositoryPort = etudiantRepositoryPort;
  }

  @Bean
  public GestionnaireCreerEtudiant gestionnaireCreerEtudiant(){
    return new GestionnaireCreerEtudiant(etudiantRepositoryPort);
  }
  @Bean
  public GestionnaireModifierEtudiant gestionnaireModifierEtudiant(){
    return new GestionnaireModifierEtudiant(etudiantRepositoryPort);
  }
  @Bean
  public GestionnaireSupprimerEtudiant gestionnaireSupprimerEtudiant(){
    return new GestionnaireSupprimerEtudiant(etudiantRepositoryPort);
  }
}

