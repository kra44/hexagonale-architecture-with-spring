package com.hexagone.presenter.facade;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.domain.model.EtudiantModel;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

public interface EtudiantUsecaseFacade {
  EtudiantModel creerEtudiant(CreerEtudiantCommande commande);
  EtudiantModel modifierEtudiant(ModifierEtudiantCommande commande);
  void supprimerEtudiant(SupprimerEtudiantCommande commande);

}
