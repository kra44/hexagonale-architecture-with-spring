package com.hexagone.presenter.facade;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.application.commande.note.CreeNoteCommande;
import com.hexagone.application.commande.note.ModifierNoteCommande;
import com.hexagone.application.commande.note.SupprimerNoteCommande;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */

public interface NoteUseCaseFacade {
  NoteModel creerNote(CreeNoteCommande commande);
  NoteModel modifierNote(ModifierNoteCommande commande);
  void supprimerNote(SupprimerNoteCommande commande);
}
