package com.hexagone.presenter.facade.impl;

import com.hexagone.application.commande.note.CreeNoteCommande;
import com.hexagone.application.commande.note.ModifierNoteCommande;
import com.hexagone.application.commande.note.SupprimerNoteCommande;
import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.gestionnairecommande.note.GestionnaireCreerNote;
import com.hexagone.application.gestionnairecommande.note.GestionnaireModifierNote;
import com.hexagone.application.gestionnairecommande.note.GestionnaireSupprimerNote;
import com.hexagone.application.ports.NoteRepositoryPort;
import com.hexagone.application.utils.Converter;

import com.hexagone.domain.model.NoteModel;
import com.hexagone.presenter.facade.NoteUseCaseFacade;
import org.springframework.stereotype.Service;

/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */
@Service
public class NoteFacadeUseCaseImpl implements NoteUseCaseFacade {
  private final GestionnaireCommande<CreeNoteCommande> creeNoteCommandeGestionnaireCommande;
  private final GestionnaireCommande<ModifierNoteCommande> modifierNoteCommandeGestionnaireCommande;
  private final GestionnaireCommande<SupprimerNoteCommande> supprimerNoteCommandeGestionnaireCommande;

  public NoteFacadeUseCaseImpl(NoteRepositoryPort noteRepositoryPort){
    this.creeNoteCommandeGestionnaireCommande = new GestionnaireCreerNote(noteRepositoryPort);
    this.supprimerNoteCommandeGestionnaireCommande = new GestionnaireSupprimerNote(noteRepositoryPort);
    this.modifierNoteCommandeGestionnaireCommande = new GestionnaireModifierNote(noteRepositoryPort);
  }

  @Override
  public NoteModel creerNote(CreeNoteCommande commande) {
    NoteModel noteModel = Converter.convertirCreerNoteVersNoteModel(
        this.creeNoteCommandeGestionnaireCommande.execute(commande)
    );
    return noteModel;
  }

  @Override
  public NoteModel modifierNote(ModifierNoteCommande commande) {
    NoteModel noteModel = Converter.convertirCreerNoteVersNoteModel(
        this.creeNoteCommandeGestionnaireCommande.execute(commande)
    );
    return noteModel;
  }

  @Override
  public void supprimerNote(SupprimerNoteCommande commande) {
     this.supprimerNoteCommandeGestionnaireCommande.execute(commande);
  }
}

