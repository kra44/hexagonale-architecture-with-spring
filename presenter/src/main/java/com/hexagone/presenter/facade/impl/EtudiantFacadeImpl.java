package com.hexagone.presenter.facade.impl;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.application.gestionnairecommande.GestionnaireCommande;
import com.hexagone.application.gestionnairecommande.etudiant.GestionnaireCreerEtudiant;
import com.hexagone.application.gestionnairecommande.etudiant.GestionnaireModifierEtudiant;
import com.hexagone.application.gestionnairecommande.etudiant.GestionnaireSupprimerEtudiant;
import com.hexagone.application.ports.EtudiantRepositoryPort;
import com.hexagone.application.utils.Converter;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.presenter.facade.EtudiantUsecaseFacade;

import org.springframework.stereotype.Service;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

@Service
public class EtudiantFacadeImpl implements EtudiantUsecaseFacade {
  private  GestionnaireCommande<CreerEtudiantCommande> creerEtudiantCommandeGestionnaireCommande;
  private  GestionnaireCommande<ModifierEtudiantCommande> modifierEtudiantCommandeGestionnaireCommande;
  private  GestionnaireCommande<SupprimerEtudiantCommande> supprimerEtudiantCommandeGestionnaireCommande;


  public EtudiantFacadeImpl(EtudiantRepositoryPort etudiantRepositoryPort){
    this.creerEtudiantCommandeGestionnaireCommande = new GestionnaireCreerEtudiant(etudiantRepositoryPort);
    this.modifierEtudiantCommandeGestionnaireCommande = new GestionnaireModifierEtudiant(etudiantRepositoryPort);
    this.supprimerEtudiantCommandeGestionnaireCommande = new GestionnaireSupprimerEtudiant(etudiantRepositoryPort);
  }
  @Override
  public EtudiantModel creerEtudiant(CreerEtudiantCommande commande) {
    EtudiantModel etudiantModel = Converter.convertiCeerEtudiantCommandeVersEtudiantModel(
        this.creerEtudiantCommandeGestionnaireCommande.execute(commande)
    );
     return etudiantModel;
  }

  @Override
  public EtudiantModel modifierEtudiant(ModifierEtudiantCommande commande) {
    EtudiantModel etudiantModel = Converter.convertiModifierVersEtudiantModel(
        this.modifierEtudiantCommandeGestionnaireCommande.execute(commande)
    );
    return etudiantModel;
  }

  @Override
  public void supprimerEtudiant(SupprimerEtudiantCommande commande) {
     this.supprimerEtudiantCommandeGestionnaireCommande.execute(commande);
  }
}

