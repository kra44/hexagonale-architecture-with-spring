package com.hexagone.presenter.controller;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.infrastructure.service.GetEtudiantByCriteriaService;
import com.hexagone.presenter.facade.EtudiantUsecaseFacade;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

@RestController
@CrossOrigin("*")
public class EtudiantController {
   private final EtudiantUsecaseFacade etudiantUsecaseFacade;
   private final GetEtudiantByCriteriaService getEtudiantByCriteriaService;

  public EtudiantController(EtudiantUsecaseFacade etudiantUsecaseFacade,
      GetEtudiantByCriteriaService getEtudiantByCriteriaService) {
    this.etudiantUsecaseFacade = etudiantUsecaseFacade;
    this.getEtudiantByCriteriaService = getEtudiantByCriteriaService;
  }

  @PostMapping("/")
  public EtudiantModel creerEtudiant(@RequestBody CreerEtudiantCommande commande){
    return this.etudiantUsecaseFacade.creerEtudiant(commande);
  }
  @PutMapping("/")
  public EtudiantModel modifierEtudiant(@RequestBody ModifierEtudiantCommande commande){
     return this.etudiantUsecaseFacade.modifierEtudiant(commande);
  }
  @GetMapping("/{id}")
  public void supprimerEtudiant(@PathVariable("id") Long id, @RequestBody SupprimerEtudiantCommande commande){
     commande = new SupprimerEtudiantCommande(id);
    this.etudiantUsecaseFacade.supprimerEtudiant(commande);
  }

  @PostMapping("/getAll")
  public List<EtudiantModel> getByCriteria(@RequestBody EtudiantModel etudiantModel){
    return this.getEtudiantByCriteriaService.requete(etudiantModel);
  }
}

