package com.hexagone.presenter.controller;

import com.hexagone.application.commande.etudiant.CreerEtudiantCommande;
import com.hexagone.application.commande.etudiant.ModifierEtudiantCommande;
import com.hexagone.application.commande.etudiant.SupprimerEtudiantCommande;
import com.hexagone.application.commande.note.CreeNoteCommande;
import com.hexagone.application.commande.note.ModifierNoteCommande;
import com.hexagone.application.commande.note.SupprimerNoteCommande;
import com.hexagone.domain.model.EtudiantModel;
import com.hexagone.domain.model.NoteModel;
import com.hexagone.presenter.facade.NoteUseCaseFacade;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
//inacasy
/**
 * @author daniel.kouame
 * @created 12/05/2023
 * @project architecture
 */
@RestController
@CrossOrigin("*")
public class NoteController {
  private final NoteUseCaseFacade noteUseCaseFacade;

  public NoteController(NoteUseCaseFacade noteUseCaseFacade) {
    this.noteUseCaseFacade = noteUseCaseFacade;
  }
  @PostMapping("/note")
  public NoteModel creerNote(@RequestBody CreeNoteCommande commande){
    return this.noteUseCaseFacade.creerNote(commande);
  }
  @PutMapping("/note")
  public NoteModel modifierNote(@RequestBody ModifierNoteCommande commande){
    return this.noteUseCaseFacade.modifierNote(commande);
  }
  @GetMapping("/note/{id}")
  public void supprimerNote(@PathVariable("id") Long id, @RequestBody SupprimerNoteCommande commande){
    commande = new SupprimerNoteCommande(id);
    this.noteUseCaseFacade.supprimerNote(commande);
  }
}

