package com.hexagone.domain.exception;

/**
 * @author daniel.kouame
 * @created 11/05/2023
 * @project architecture
 */

public class EtudiantException extends RuntimeException{
  public EtudiantException(String message){
    super(message);
  }

}

