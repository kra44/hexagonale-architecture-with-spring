package com.hexagone.domain.model;

import java.time.Instant;

public class NoteModel {
  private Long id;
  private Double note;
  private EtudiantModel etudiant;
  private Instant createDate;
  private Instant updateDate;
  private Integer status;

  public NoteModel() {
  }

  public NoteModel(Long id, Double note, EtudiantModel etudiant, Instant createDate,
      Instant updateDate, Integer status) {
    this.id = id;
    this.note = note;
    this.etudiant = etudiant;
    this.createDate = createDate;
    this.updateDate = updateDate;
    this.status = status;
  }

  public NoteModel(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getNote() {
    return note;
  }

  public void setNote(Double note) {
    this.note = note;
  }

  public EtudiantModel getEtudiant() {
    return etudiant;
  }

  public void setEtudiant(EtudiantModel etudiant) {
    this.etudiant = etudiant;
  }

  public Instant getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Instant createDate) {
    this.createDate = createDate;
  }

  public Instant getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Instant updateDate) {
    this.updateDate = updateDate;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }
}
